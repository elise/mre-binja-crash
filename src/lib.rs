use binaryninja::{binaryview::{BinaryView, BinaryViewBase}, custombinaryview::{CustomBinaryView, BinaryViewType, BinaryViewTypeBase, CustomBinaryViewType}};

pub struct StubbedView {
    pub handle: binaryninja::rc::Ref<binaryninja::binaryview::BinaryView>,
    // Comment out for the binary to no longer crash
    pub x: Box<()>,
}

impl AsRef<BinaryView> for StubbedView {
    fn as_ref(&self) -> &BinaryView {
        &self.handle
    }
}

impl BinaryViewBase for StubbedView {
    fn entry_point(&self) -> u64 {
        0
    }

    fn default_endianness(&self) -> binaryninja::Endianness {
        binaryninja::Endianness::LittleEndian
    }

    fn address_size(&self) -> usize {
        8
    }
}

unsafe impl CustomBinaryView for StubbedView {
    type Args = ();

    fn new(handle: &BinaryView, _args: &Self::Args) -> binaryninja::binaryview::Result<Self> {
        Ok(Self {
            handle: handle.to_owned(),
            x: Box::new(()),
        })
    }

    fn init(&self, _args: Self::Args) -> binaryninja::binaryview::Result<()> {
        Ok(())
    }
}

pub struct StubbedViewType {
    pub handle: BinaryViewType,
}

impl AsRef<BinaryViewType> for StubbedViewType {
    fn as_ref(&self) -> &BinaryViewType {
        &self.handle
    }
}

impl BinaryViewTypeBase for StubbedViewType {
    fn is_valid_for(&self, _data: &BinaryView) -> bool {
        true
    }
}

impl CustomBinaryViewType for StubbedViewType {
    fn create_custom_view<'builder>(
        &self,
        data: &binaryninja::binaryview::BinaryView,
        builder: binaryninja::custombinaryview::CustomViewBuilder<'builder, Self>,
    ) -> binaryninja::binaryview::Result<binaryninja::custombinaryview::CustomView<'builder>> {
        builder.create::<StubbedView>(data, ())
    }
}

#[no_mangle]
pub extern "C" fn UIPluginInit() -> bool {
    binaryninja::custombinaryview::register_view_type(
        "StubbedView",
        "Stubbed View",
        |handle| {
            StubbedViewType { handle }
        },
    );
    true
}
